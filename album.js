import { Product } from "./product.js";
class Album extends Product {
    constructor(name, price, numberOfCopies, artist) {
        super(name, price, numberOfCopies);
        this.artist = artist;
    }

    getAlbumInfo() {
        return `Album: ${this.name}, Artist: ${this.artist}, Price: ${this.price}, Copies: ${this.numberOfCopies}`;
    }
}
export { Album}