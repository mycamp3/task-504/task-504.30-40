import { Product } from "./product.js";
class Movie extends Product {
    constructor(name, price, numberOfCopies, director) {
        super(name, price, numberOfCopies);
        this.director = director;
    }

    getMovieInfo() {
        return `Movie: ${this.name}, Director: ${this.director}, Price: ${this.price}, Copies: ${this.numberOfCopies}`;
    }
}

export { Movie}