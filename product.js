class Product {
    constructor(name, price, numberOfCopies) {
        this.name = name;
        this.price = price;
        this.numberOfCopies = numberOfCopies;
    }

    sellCopies(num) {
        if (num > this.numberOfCopies) {
            console.log(`Không đủ bản sao để bán. Chỉ còn ${this.numberOfCopies} bản sao.`);
        } else {
            this.numberOfCopies -= num;
            console.log(`Đã bán ${num} bản sao. Số bản sao còn lại: ${this.numberOfCopies}.`);
        }
    }

    orderCopies(num) {
        this.numberOfCopies += num;
        console.log(`Đã đặt thêm ${num} bản sao. Tổng số bản sao hiện có: ${this.numberOfCopies}.`);
    }
}

export { Product}