import { Product } from "./product.js";
import { Album } from "./album.js";
import { Movie } from "./movie.js";

const product1 = new Product('Introduction to JavaScript', 19.99, 100);

console.log(`Tên sản phẩm: ${product1.name}`);
console.log(`Giá sản phẩm: ${product1.price}`);
console.log(`Số lượng bản sao hiện có: ${product1.numberOfCopies}`);

product1.sellCopies(10);

product1.orderCopies(20);

product1.sellCopies(200);

console.log(product1 instanceof Product); // true



const album1 = new Album('1989', 15.99, 500, 'Taylor Swift');
console.log(album1.getAlbumInfo());
album1.sellCopies(50);
album1.orderCopies(30);
console.log(album1.getAlbumInfo());

console.log(album1 instanceof Album); // true
console.log(album1 instanceof Product); // true


const movie1 = new Movie('The Dark Knight', 25.99, 900, 'Christopher Nolan');
console.log(movie1.getMovieInfo());
movie1.sellCopies(100);
movie1.orderCopies(200);
console.log(movie1.getMovieInfo());

console.log(movie1 instanceof Movie); // true
console.log(movie1 instanceof Product); // true
